if exists('g:vim_skeleton')
	finish
endif
let g:vim_skeleton = 1

if !exists('g:skeleton_path')
	let g:skeleton_path = '~/.vim/templates'
endif

if !exists('g:skeleton_prefix')
	let g:skeleton_prefix = 'skeleton.'
endif

if !exists('g:skeleton_cmds')
	let g:skeleton_cmds = {}
endif

augroup skeletons
	au!
	autocmd BufNewFile *.* :call Skeleton()
augroup END

function! Skeleton()
	let l:ext  = expand("<afile>:e")
	let l:file = expand(g:skeleton_path . '/' . g:skeleton_prefix . l:ext)
	let l:src  = expand(g:skeleton_path . '/' . l:ext . '.vim')

	if !filereadable(l:file)
		return
	endif

	" clean up buffer list
	execute ':keepalt 0r ' . l:file
	execute ':bwipeout ' . l:file
	normal Gdd

	if filereadable(l:src)
		execute 'source ' . l:src
	endif

	if has_key(g:skeleton_cmds, l:ext)
		execute g:skeleton_cmds[l:ext]
	endif
endfunction
